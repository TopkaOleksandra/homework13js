//Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
//Метод setTimeout() запускається один раз при визові, setInterval()`використовується якщо
// необхідне регулярне виконання через визначений період часу.
//Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
//виконання методу якнайшвидше,запуск одразу після поточного скрипту, браузер обмежує мін затримку для
// перших викликів setTimeout(після 5 -го виклику) до 4 мс.
//Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл
// запуску вам вже не потрібен?
//Якщо не видаляти таймер, то буде втрачатись пам'ять, а також можуть бути
// помилки із-за того що метод буде неочікувано виконуватись.

let img = document.querySelectorAll('.image-to-show');
let firstImage = img[0];
let lastImage = img[img.length - 1];
const slider = () => {const showImage = document.querySelector('.visible');
    {if (showImage!==lastImage) {showImage.classList.remove('visible');
        showImage.nextElementSibling.classList.add('visible');}
    else {firstImage.classList.add('visible');
        showImage.classList.remove('visible')}
    }};
let timer = setInterval(slider, 3000);

let stopBtn = document.querySelector('.stop');
stopBtn.addEventListener('click', () => {clearInterval(timer);}, true);

let startBtn = document.querySelector('.start');
startBtn.addEventListener('click', () => {timer = setInterval(slider, 3000);}, false)